\contentsline {section}{\numberline {1}Abstract}{4}{}%
\contentsline {section}{\numberline {2}Acknowledgement}{5}{}%
\contentsline {section}{\numberline {3}Introduction}{6}{}%
\contentsline {subsection}{\numberline {3.1}Objective of the project}{6}{}%
\contentsline {section}{\numberline {4}Background}{7}{}%
\contentsline {subsection}{\numberline {4.1}Need for predicting the solubility of compounds}{7}{}%
\contentsline {subsection}{\numberline {4.2}Understanding Delaney's work}{7}{}%
\contentsline {subsection}{\numberline {4.3}Molecular Descriptors used by Delaney}{7}{}%
\contentsline {section}{\numberline {5}Methodology}{9}{}%
\contentsline {subsection}{\numberline {5.1}Data and Tools used}{9}{}%
\contentsline {subsection}{\numberline {5.2}Implementation}{10}{}%
\contentsline {subsection}{\numberline {5.3}Explanation}{10}{}%
\contentsline {section}{\numberline {6}Results}{11}{}%
\contentsline {subsection}{\numberline {6.1}Results of train data}{11}{}%
\contentsline {subsection}{\numberline {6.2}Results of test data}{11}{}%
\contentsline {subsection}{\numberline {6.3}Linear Regression Equation}{11}{}%
\contentsline {section}{\numberline {7}Conclusion}{13}{}%
\contentsline {section}{\numberline {8}Future Work}{14}{}%
