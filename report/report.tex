\documentclass[12pt, a4paper]{article}
%\renewcommand{\baselinestretch}{1.1}
\usepackage{graphicx} % Required for inserting images
\usepackage{titling}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{minted}
\usepackage[margin=0.80in]{geometry}
\usepackage{pgfplots}
\usepackage{biblatex}
\usepackage{titlesec}
\usepackage[document]{ragged2e}
\usepackage{indentfirst}
%\usepackage{ulem}
\usepackage{tabularx}
\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

\newcommand{\sectionbreak}{\clearpage}
\usepackage[utf8]{inputenc}
\addbibresource{references.bib}
\pgfplotsset{width=13cm,compat=1.9}

\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}

\title{\textbf{{\large DEPARTMENT OF COMPUTER ENGINEERING} \\ \vspace{1.5em} {\small A Mini Project Report on} \\ \vspace{0.5em} MACHINE LEARNING} \\ \subtitle{\textbf{Cheminformatics in Python: Predicting the Solubility of Molecules}}}

\begin{document}

\begin{figure}
    \centering
    \includegraphics[width = 15cm]{./images/aissmslogo.jpeg}
    \vspace*{-1.5cm}
\end{figure}
\date{}
\maketitle
\vspace{-6.5em}
\begin{center}
    \textit{Submitted in partial fulfilment of the requirements for the degree of}
    \\ \vspace{1.5em}
    \textbf{BACHELOR OF ENGINEERING \\ in \\ COMPUTER ENGINEERING}
    \\ \vspace{0.5em}
    \textit{Submitted by}
    \\ \vspace{0.5em}
    \textbf{SWANAND MODAK (20CO073) \\ PRASANNA SHINDE (20CO124) \\ ANIKET TIWARI (20CO135)}
    \\ \vspace{1.5em}
    Under the Guidance of 
    \\ \vspace{0.5em}
    \textbf{Prof. D. M. Ujalambkar}
    \\ \vspace{2em}
    Academic Year 2023 - 24 (Term - I)
    \\ \vspace{0.5em}
    \textbf{Savitribai Phule Pune University}
\end{center}

\newpage
\begin{figure}
    \centering
    \includegraphics[width = 15cm]{./images/aissmslogo.jpeg}
    \vspace*{0.5em}
\end{figure}

\begin{center}
    \textbf{\large DEPARTMENT OF COMPUTER ENGINEERING}
    \\ \vspace{2em}
    \textbf{\Huge CERTIFICATE}
    \\ \vspace{2.5em}
\end{center}

\justifying
{This is to certify that \textbf{ANIKET SANTOSH TIWARI (20CO135)} from \textbf{Final Year, Computer Engineering} has successfully completed his mini project report titled \textbf{``Cheminformatics in Python: Predicting the Solubility of Molecules"} at AISSMS College of Engineering, Pune as part of the curriculum of the subject Machine Learning.} \\

\vspace{8cm}
\begin{center}
    \begin{tabularx}{1.0\textwidth}{>{\centering\arraybackslash}X  >{\centering\arraybackslash}X >{\centering\arraybackslash}X}  
        \textbf{Prof. D. M. Ujalambkar} & \textbf{Dr. S. V. Athawale} & \textbf{Dr. D. S. Bormane} \\ 
        Faculty Guide & HOD & Principal \\ 
        Computer Engineering & Computer Engineering & AISSMS COE, Pune \\
    \end{tabularx}
\end{center}


\tableofcontents
 
\section{Abstract}
\justifying
Drug solubility is a critical physicochemical property that affects the bioavailability, formulation, and development of drugs. 
Experimental measurement of drug solubility is time-consuming and expensive, making it a bottleneck in the drug discovery and development process. 
Machine learning (ML) offers a promising approach to predicting drug solubility quickly and accurately.

This project aims to develop a robust and generalizable ML model to predict the solubility of drugs in a wide range of solvents. 
The model will be trained on a comprehensive dataset of drug solubility measurements and molecular descriptors, such as molecular weight, polarity, functional groups, and topological indices. 
ML algorithms such as random forests, gradient boosting machines, and deep neural networks will be explored to develop the most accurate and efficient model.

\section{Acknowledgement}
It gives me great pleasure to acknowledge the contribution of all those who have helped directly or indirectly to the completion of the project.

\vspace{0.5em}
I express my foremost and deepest gratitude to my guide \textbf{Prof. D. M. Ujalambkar} for her supervision, noble guidance and constant encouragement in carrying out the project work.

\vspace{0.5em}
I am deeply indebted to \textbf{Dr. S. V. Athawale}, HOD, Computer ENgineering Department for his time to time advice and support and also for providing all the necessary facilities available in the institute.

\vspace{0.5em}
I also acknowledge all the teaching and non-teaching staff of the Computer Engineering Department for redering their support directly or otherwise. It would not have been possible to complete this project without their support.


\section{Introduction}
In this mini project, we will dive into the world of Cheminformatics which lies at the interface of Informatics and Chemistry. We will be reproducing a research article (by John S. Delaney) by applying Linear Regression to predict the solubility of molecules (i.e. solubility of drugs is an important physicochemical property in Drug discovery, design and development).
This idea for this notebook was inspired by the excellent blog post by Pat Walters where he reproduced the linear regression model with similar degree of performance as that of Delaney. 
This example is also briefly described in the book Deep Learning for the Life Sciences: Applying Deep Learning to Genomics, Microscopy, Drug Discovery, and More.


\subsection{Objective of the project}
\begin{enumerate}
    \item To verify the accuracy of the results. Even the best scientists make mistakes, and it is possible that the original study had errors in its methodology or data analysis. Reproducing the study can help to identify and correct these errors.
    \item To test the generality of the findings. The original study may have been conducted on a specific sample population or under specific conditions. Reproducing the study with a different sample population or under different conditions can help to determine whether the findings are generalizable to other settings.
    \item To build on the original research. Once the findings of a paper have been reproduced, other scientists can use this knowledge to design new experiments and develop new theories. This can help to advance scientific knowledge more quickly.
    \item To improve transparency and accountability in science. By reproducing other scientists' work, researchers can help to ensure that scientific findings are reliable and reproducible. This can help to build public trust in science and reduce the risk of fraud.
\end{enumerate}

\section{Background}
\subsection{Need for predicting the solubility of compounds}
As explained earlier, the solubility of a compound is important for a variety of reasons. Solubility is a key factor that determines how much of a drug is absorbed into the bloodstream after administration. Drugs that are poorly soluble are less likely to be absorbed, which can reduce their efficacy. This is commonly termed as bioavailability.
Solubility also affects how drugs can be formulated into different dosage forms, such as tablets, capsules, and injections.
Drugs that are poorly soluble may be difficult to formulate into stable and effective dosage forms.
Solubility can also affect the toxicity of drugs. Drugs that are poorly soluble are more likely to crystallize in the kidneys or other tissues, which can lead to serious side effects. 
Therefore it is important to predict the solubility of the compounds. This will help us to:
\begin{enumerate}
    \item Identify drug candidates with good bioavailability
    \item Design drug formulations that are stable and effective
    \item Predict the potential toxicity of drugs
\end{enumerate}

\subsection{Understanding Delaney's work}
Delaney's work on predicting the solubility of chemicals has been highly influential in the field.
He developed a number of machine learning models that can accurately predict the solubility of compounds in a wide range of solvents, including water, organic solvents, and supercritical fluids.
One of Delaney's most important contributions was the development of the Estimating Aqueous Solubility Directly from Molecular Structure (ESOL) model.
This model is based on a set of simple molecular descriptors, such as molecular weight, polarity, and functional groups.
The ESOL model is easy to use and can be applied to a wide range of compounds.
It has been shown to be accurate in predicting the solubility of compounds in water with a mean absolute error of prediction of less than 0.5 log units.

Delaney has also developed some other models like the following:
\begin{itemize}
    \item The COSMO-RS model: This model is based on a quantum mechanical description of molecular interactions. It is more accurate than the ESOL model, but it is also more complex and computationally expensive.
    \item The Abraham model: This model is based on solvation energy relationships. It is less accurate than the ESOL model, but it is also less complex and computationally expensive.
    \item The Yaffe model: This model is a fuzzy ARTMAP model that is based on a combination of molecular descriptors and experimental solubility data. It is more accurate than the ESOL model, but it is also more complex and computationally expensive.
\end{itemize}
Delaney's work in predicting the solubility of chemicals has had a significant impact on the field. His models are used by pharmaceutical companies, drug formulation scientists, and regulatory agencies to improve the drug discovery and development process and to ensure that drugs are safe and effective.


\subsection{Molecular Descriptors used by Delaney}
To calculate the solubility of the compounds Delaney uses 4 molecular descriptors:
\begin{enumerate}
    \item cLogP (Octanol-water partition coefficient)
    \item MW (Molecular weight)
    \item RB (Number of rotatable bonds)
    \item AP (Aromatic proportion = number of aromatic atoms / total number of heavy atoms)
\end{enumerate}

\section{Methodology}
\subsection{Data and Tools used}
\textbf{Data:}
The dataset used by Delaney for his paper ``ESOL: Estimating Aqueous Solubility Directly from Molecular Structure" consists of 2874 aqueous solubility measurements for a variety of organic compounds.
The solubility measurements are reported in log units (mol/L). The dataset also includes a set of nine molecular descriptors for each compound, including:
\begin{enumerate}
    \item Molecular weight
    \item Proportion of heavy atoms in aromatic systems
    \item Number of rotatable bonds
    \item Number of hydrogen bond donors
    \item Number of hydrogen bond acceptors
    \item Molar refractivity
    \item LogP (octanol-water partition coefficient)
    \item Polar surface area
    \item Eccentricity
    \item Topological polar surface area
\end{enumerate}
Delaney's dataset is a valuable resource for cheminformatics researchers who are developing machine learning models to predict the aqueous solubility of organic compounds.
The dataset is well-balanced and representative of a wide range of organic compounds.

\textbf{Tools and Programming language used:}
\begin{enumerate}
    \item Programming language: \hfill \\ For the project, we have chosen Python as our programming language due to its simplicity and availability of libraries. We used Jupyter Notebook as our Integrated Development Environment(IDE).
    \\Following python libraries are used for comparing Naive and Rabin-Karp string matching algorithm:

    \begin{enumerate}
        \item \textbf{Matplotlib:} This library is great for creating plots and graphs to visualize your data and compare the performance of the algorithms.
        \item \textbf{NumPy:} umPy is essential for numerical computations and handling arrays. You may use it for efficient data handling and calculations.
        \item \textbf{RDKit:} An open-source cheminformatics toolkit.
        \item \textbf{Scikit-learn:} A free, open-source machine learning library for Python.
    \end{enumerate}
    \item Data Visualization: \hfill \\ Considered data visualization libraries like matplotlib and created a graph to illustrate the results comparison of time complexity of both algorithms.
\end{enumerate}
\clearpage

\subsection{Implementation}
Predicting the X\_train:

\begin{minted}{python}
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
model = linear_model.LinearRegression()
model.fit(X_train, Y_train)
Y_pred_train = model.predict(X_train)
print('Coefficients:', model.coef_)
print('Intercept:', model.intercept_)
print('Mean squared error (MSE): %.2f'
      % mean_squared_error(Y_train, Y_pred_train))
print('Coefficient of determination (R^2): %.2f'
      % r2_score(Y_train, Y_pred_train))
\end{minted}

Predicting the X\_test:

\begin{minted}{python}
Y_pred_test = model.predict(X_test)
print('Coefficients:', model.coef_)
print('Intercept:', model.intercept_)
print('Mean squared error (MSE): %.2f'
      % mean_squared_error(Y_test, Y_pred_test))
print('Coefficient of determination (R^2): %.2f'
      % r2_score(Y_test, Y_pred_test))
\end{minted}

\subsection{Explanation}
We have implemented linear regression to predict the solubility of the drugs after splitting the dataset.
Now the same process is repeated for working with the entire dataset to get the accuracy of the model improved.
The results to theses are shown later.

\section{Results}
\subsection{Results of train data}
\begin{minted}{python}
Coefficients: [-0.75114255 -0.00662895  0.00811978 -0.44887394]
Intercept: 0.2763526517630188
Mean squared error (MSE): 1.01
Coefficient of determination (R^2): 0.77
\end{minted}

\subsection{Results of test data}
\begin{minted}{python}
Coefficients: [-0.75114255 -0.00662895  0.00811978 -0.44887394]
Intercept: 0.2763526517630188
Mean squared error (MSE): 1.01
Coefficient of determination (R^2): 0.75
\end{minted}

\subsection{Linear Regression Equation}
The work of Delaney provided us with the following equation:
$$ LogS = 0.16 -  0.63 cLogP - 0.0062 MW + 0.066 RB - 0.74 AP $$

The notebook by Pat Walters Provided us with the following equation:
$$ LogS = 0.26 -  0.74 LogP - 0.0066 MW + 0.0034 RB - 0.42 AP $$

Our notebook gives us the following equations:

Based on the train dataset:
$$ LogS = 0.30 -0.75 LogP - .0066 MW -0.0041 RB - 0.36 AP $$

Based on the full dataset:
$$ LogS =  0.26 -0.74 LogP - 0.0066 MW + 0.0032 RB - 0.42 AP $$

\newpage

\begin{figure}
    \centering
    \includegraphics[width=15cm]{./images/regression.png}
\end{figure}

\section{Conclusion}
The successful reproduction of Delaney's work with even greater accuracy is a significant achievement. It demonstrates that the machine learning approach to predicting the solubility of chemicals is valid and has the potential to be even more accurate than Delaney's original models.

The improved accuracy of the new models has several potential benefits. First, it will allow scientists to screen drug candidates for solubility more accurately and efficiently. This could lead to the discovery of new drugs that are more soluble and therefore more bioavailable. Second, the improved models can be used to design drug formulations that are more stable and effective. Finally, the improved models can be used to predict the solubility of chemicals in different biological fluids, which could help to improve the safety of drugs and other chemicals.

Overall, the successful reproduction of Delaney's work with even greater accuracy is a significant advance in the field of cheminformatics. It has the potential to accelerate the drug discovery and development process, improve the design of drug formulations, and enhance the safety of drugs and other chemicals.


\section{Future Work}
Here are some specific examples of how the new models could be used:
\begin{enumerate}
    \item Drug discovery: Pharmaceutical companies could use the new models to screen drug candidates for solubility early in the drug discovery process. This would help to identify drug candidates with good bioavailability and to avoid developing drugs that are poorly soluble.
    \item Drug formulation: Drug formulation scientists could use the new models to design drug formulations that are more stable and effective. For example, the new models could be used to select excipients that improve the solubility of a drug or to design delivery systems that protect the drug from degradation.
    \item Safety assessment: Regulatory agencies could use the new models to predict the solubility of drugs in different biological fluids. This information could be used to assess the potential toxicity of drugs and to set safe dosage levels.
\end{enumerate}

\nocite{plaxton2005string} \nocite{sri2018string} \nocite{pandey2014study} \nocite{chillar2008rb} \nocite{rasool2012string} \nocite{faro2016string} \nocite{colussi1990exact} \nocite{singla2012string}
\printbibliography

\end{document}
